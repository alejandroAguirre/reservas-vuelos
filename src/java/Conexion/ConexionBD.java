/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;
import java.sql.*;

/**
 *
 * @author Alejandro
 */
public class ConexionBD {
    
    Connection conexion = null;
    
    public Connection conectar()
    {    
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/reservacion_vuelos","root","");    
            System.out.print("Conectado");
        } catch (Exception e) 
        {
            System.out.print("Error: "+e.getMessage());
        }
        return conexion;
    } 
    
    /*
    public static void main(String arg[]){
       ConexionBD c = new ConexionBD(); 
        System.out.println(c.conectar());
    }
    */
}
