<%-- 
    Document   : index.jsp
    Created on : 23-sep-2018, 22:28:43
    Author     : Alejandro
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
 // define el titulo a mostrar en la pagina.
 String sTitle = "Principal";
 if (request.getParameter("title") != null) { 
   sTitle = request.getParameter("title").toString();
 }
 // define la pagina a desplegar de acuerdo al parametro "page" del request.
 String sUrlPage = "template/contenido/home.jsp";
 if (request.getParameter("page") != null) { 
   sUrlPage = "template/contenido/"+request.getParameter("page").toString()+".jsp";
 }
 %>
  
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%=sTitle%></title>
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <link href="css/animate.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        
        <!-- jquery-3.2.1 -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>
        <div id="wrapper">
            <jsp:include page="template/side_menu.jsp" />
            <div id="page-wrapper" class="gray-bg">
                <jsp:include page="template/header.jsp" />

                <!-- contenido -->
                <jsp:include page="<%=sUrlPage%>" />


                <jsp:include page="template/footer.jsp" />
            </div>
        </div>
            
    </body>
</html>
