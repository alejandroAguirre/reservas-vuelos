<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Reservar Vuelo</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">
                <strong>Reservar vuelo</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-7">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Registro</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal">
                        <p>Si no tienes cuenta, registrate para poder reservar vuelos. </p>
                        <div class="form-group"><label class="col-lg-2 control-label">Tipo de documento</label>
                            <div class="col-sm-10">
                                    <select class="form-control m-b" name="tipo_documento">
                                    <option>option 1</option>
                                    <option>option 2</option>
                                    <option>option 3</option>
                                    <option>option 4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">N�mero de documento</label>
                            <div class="col-lg-10"><input type="text" class="form-control" name="nro_documento">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Nombres</label>
                            <div class="col-lg-10"><input type="text" class="form-control" name="nombres">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Apellidos</label>
                            <div class="col-lg-10"><input type="text" class="form-control" name="apellidos">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Edad</label>
                            <div class="col-lg-10"><input type="text" class="form-control" name="edad">
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Email</label>

                            <div class="col-lg-10"><input type="email" placeholder="Email" class="form-control"> <span class="help-block m-b-none">Example block-level help text here.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Password</label>

                            <div class="col-lg-10"><input type="password" placeholder="Password" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-sm btn-white" type="submit">Registrarse</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="col-lg-5">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Iniciar sesi�n</h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal">
                        <p>Si ya tienes una cuenta, incia sesi�n.</p>
                        <div class="form-group"><label class="col-lg-2 control-label">Email</label>

                            <div class="col-lg-10"><input type="email" placeholder="Email" class="form-control"> <span class="help-block m-b-none">Example block-level help text here.</span>
                            </div>
                        </div>
                        <div class="form-group"><label class="col-lg-2 control-label">Password</label>

                            <div class="col-lg-10"><input type="password" placeholder="Password" class="form-control"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <div class="i-checks"><label> <input type="checkbox"><i></i> Remember me </label></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-offset-2 col-lg-10">
                                <button class="btn btn-sm btn-white" type="submit">Ingresar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>