<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Consultar vuelos</h2>
        <ol class="breadcrumb">
            <li>
                <a href="index.html">Home</a>
            </li>
            <li class="active">
                <strong>Consultar vuelos</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <h5>Consulte vuelos por los filtros de busqueda mostrados</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <i class="fa fa-wrench"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="#">Config option 1</a>
                                </li>
                                <li><a href="#">Config option 2</a>
                                </li>
                            </ul>
                            <a class="close-link">
                                <i class="fa fa-times"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <form role="form" class="form-inline">
                            <div class="form-group">
                                <label for="fecha_inicio" class="sr-only">Fecha inicio</label>
                                <input type="text" placeholder="Ingrese fecha de inicio" id="fecha_inicio" name="fecha_inicio"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="fecha_fin" class="sr-only">Fecha fin</label>
                                <input type="text" placeholder="Ingrese fecha de fin" id="fecha_inicio" name="fecha_fin"
                                       class="form-control">
                            </div>
                            <button class="btn btn-white" type="submit">Buscar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>